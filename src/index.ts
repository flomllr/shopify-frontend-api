import { listeners } from 'process';

/**
 * Reprecents a Shopify request.
 */
export interface IShopifyRequest {
  endpoint: string;
  method?: string;
  data?: any;
  type?: XMLHttpRequestResponseType;
}

/**
 * Reprecents a standard hashtable.
 */
export interface IHashTable {
  [name: string]: any;
}

/**
 * Ensure we get back JSON object from an XHR request.
 * @param response - XHR response data.
 */
function transformResponse(
  response: IHashTable | string,
): IHashTable {
  // tslint:disable-next-line
  return response.constructor === String
    ? JSON.parse(response as string)
    : response;
}

/**
 * Shopify API.
 */
class Shopify {
  /**
   * The base URL.
   */
  private static readonly baseURI: string = `${window.location.protocol}//${window.location.hostname}`;

  /**
   * Sends a request to cart.
   * @param params - The params for the registry.
   * @param params.method - The HTTP method.
   * @param params.endpoint - The endpoint of the URL.
   * @param params.data - The data to send to the endpoint.
   * @param params.type - The type of request.
   */
  static request({
    method = 'GET',
    endpoint,
    data,
    type = 'json',
  }: IShopifyRequest): Promise<XMLHttpRequest> {
    return new Promise((resolve, reject) => {
      // Generate the request object
      const xhr: XMLHttpRequest = new XMLHttpRequest();

      let adjustedEndpoint = endpoint;
      if (method === 'GET') {
        adjustedEndpoint += `?${Math.random()
          .toString(36)
          .substr(2, 10)}`;
      }

      // Build the request
      xhr.open(method, `${Shopify.baseURI}${adjustedEndpoint}`, true);
      xhr.responseType = type;
      if (type === 'json') {
        xhr.setRequestHeader(
          'Content-Type',
          'application/json;charset=UTF-8',
        );
      }

      // Resolve or rejust the promise, passing XHR to the promise
      xhr.onload = (): void => {
        if (xhr.status === 200) {
          resolve(xhr);
        } else {
          reject(xhr);
        }
      };

      // Send the data
      xhr.send(data ? JSON.stringify(data) : null);
    });
  }
}

/**
 * Shopify Checkout API.
 */
export class Checkout extends Shopify {
  /**
   * Gets the checkout markup.
   */
  static async checkout(): Promise<string> {
    const response = await Checkout.request({
      endpoint: '/checkout',
      type: 'text',
    });
    return response.responseText;
  }

  /**
   * Applies a discount via URL.
   * @param discount - The discount code to apply.
   */
  static async applyDiscount(
    discount: string,
  ): Promise<XMLHttpRequest> {
    /*
     * Use HEAD so we don't follow redirects.
     * Shopify seems to set some sort of server var to tell checkout
     * to apply the discount (not the discount_code cookie either).
     */
    return Checkout.request({
      method: 'HEAD',
      endpoint: `/discount/${discount}`,
    });
  }

  /**
   * Verifies the discount code.
   * @param discount - The discount code to verify.
   */
  static async verifyDiscount(
    discount?: string,
  ): Promise<boolean | IHashTable> {
    // Parse the DOM, easy way
    const response = await Checkout.checkout();
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(response, 'text/html');
    console.log(htmlDoc);
    (window as any).htmlDoc = htmlDoc;

    /**
     * Query selector shortcut.
     * @param query - The query string.
     * @param target - The node target.
     */
    const htmlQs = (query: string, target: Document = htmlDoc) => {
      return target.querySelector(query) as HTMLElement;
    };

    /**
     * Query selector innerHTML trim shortcut.
     * @param query - The query string.
     * @param target The node target.
     */
    const htmlQsi = (query: string, target: Document = htmlDoc) => {
      const elem = target.querySelector(query) as HTMLElement;
      return elem ? elem.innerHTML.trim() : undefined;
    };

    /**
     * Query selector all shortcut.
     * @param query - The query string.
     * @param target - The node target.
     */
    const htmlQsa = (query: string, target: Document = htmlDoc) => {
      return target.querySelectorAll(query) as NodeListOf<
        HTMLElement
      >;
    };

    // Check if the store uses the latest checkout, if so use the new handling method
    function getVersion() {
      if(response.includes('name="serialized-graphql"')) return "v3";
      if(response.includes('data-serialized-id="graphql-queries"')) return "v2";
      if(response.includes('data-serialized-id="graphql"')) return "v1";
      return "v0";
    }
    const version = getVersion();
    console.log("Shopify Checkout version", version);

    // VERSION 3
    if(version === "v3") {
      const element = htmlQs(`[name='serialized-graphql']`) as any;
      const stringData = element?.content;
      const data = JSON.parse(stringData);

      // Find the relevant data in the graphql response
      let discountCode: string;
      let discountAmount: number;
      let originalAmount: number;

      for (const entry of Object.values(data)) {
        const anyEntry = entry as any;
        const sellerProposal =
          anyEntry?.session?.negotiate?.result?.sellerProposal;

        // First check old schema, then new schema
        let discountLines =
          sellerProposal?.discount?.lines ??
          sellerProposal?.merchandiseDiscount?.lines;

        if (
          sellerProposal &&
          discountLines &&
          discountLines.length > 0
        ) {
          discountCode = discountLines[0]?.discount?.code;
          discountAmount = parseFloat(
            discountLines[0]?.lineAmount?.value?.amount,
          );
          originalAmount = parseFloat(
            sellerProposal?.subtotalBeforeReductions?.value ??
              sellerProposal?.subtotalBeforeTaxesAndShipping?.value
                ?.amount,
          );
          break;
        }
      }

      if (discountCode && discountAmount) {
        return {
          discountAmount,
          discountCode,
          originalAmount,
          success: true,
        };
      } else {
        return {
          success: false,
        };
      }
    }
   
    // VERSION 2
    if (version === "v2") {
      const getSerializedData = id => {
        const element = htmlQs(`[data-serialized-id='${id}']`);
        if (element) {
          return JSON.parse(
            element.getAttribute('data-serialized-value'),
          );
        }
      };

      const graphqlEndpoint = getSerializedData('graphql-endpoint');
      const graphqlQueries = getSerializedData('graphql-queries');
      const sessionToken = getSerializedData('session-token');
      const queueToken = getSerializedData('queue-token');
      const checkpointData = getSerializedData('checkpoint-data');
      const urlSearchParams = new URLSearchParams(
        window.location.search,
      );
      const queryParams = Object.fromEntries(
        urlSearchParams.entries(),
      );

      const getData = function(query) {
        const request = new XMLHttpRequest();
        request.open('POST', graphqlEndpoint);
        request.setRequestHeader('Content-Type', 'application/json');
        const variables: { [key: string]: string } = {
          sessionToken: sessionToken,
        };

        if (query.name === 'NegotiateFromSession') {
          variables.queueToken = queueToken;
          variables.checkpointData = checkpointData;
        }

        let resolve: (value: any) => void;
        let reject: (value: any) => void;

        const promise = new Promise((res, rej) => {
          resolve = res;
          reject = rej;
        });

        request.onload = () => {
          if (request.status >= 200 && request.status < 300) {
            resolve(JSON.parse(request.response));
          } else {
            reject({
              status: request.status,
              statusText: request.statusText,
            });
          }
        };

        request.send(
          JSON.stringify({
            query: query.source,
            variables,
            operationName: query.name,
            appBootstrap: true,
            ...queryParams,
          }),
        );

        return promise;
      };

      const query = graphqlQueries.find(
        q => q.name === 'NegotiateFromSession',
      );
      const data: any = await getData(query);

      (window as any).resultdata = data;

      // Find the relevant data in the graphql response
      let discountCode: string;
      let discountAmount: number;
      let originalAmount: number;

      const sellerProposal =
        data?.data?.session?.negotiate?.result?.sellerProposal;
      let discountLines = sellerProposal?.merchandiseDiscount?.lines;

      if (
        sellerProposal &&
        discountLines &&
        discountLines.length > 0
      ) {
        discountCode = discountLines[0]?.discount?.code;
        originalAmount = parseFloat(
          sellerProposal?.subtotalBeforeReductions?.value?.amount,
        );
        const amountAfterReduction = parseFloat(
          sellerProposal?.subtotalBeforeTaxesAndShipping?.value
            ?.amount,
        );

        // Prefer explicit discount amount
        discountAmount = parseFloat(discountLines[0]?.lineAmount?.value?.amount);

        // Fallback discount amount if explicit value is not avaliable
        discountAmount = discountAmount ||
          (amountAfterReduction != null
            ? originalAmount - amountAfterReduction
            : 0);
      }

      if (discountCode && discountAmount) {
        return {
          discountAmount,
          discountCode,
          originalAmount,
          success: true,
        };
      } else {
        return {
          success: false,
        };
      }
    }
    
    // VERSION 1
    if (version === "v1") {
      // Check if the store is using a version of checkout that includes the graphql result in the html
      const data = JSON.parse(
        htmlQs('[data-serialized-id="graphql"]').dataset
          .serializedValue,
      );

      // Find the relevant data in the graphql response
      let discountCode: string;
      let discountAmount: number;
      let originalAmount: number;

      for (const entry of Object.values(data)) {
        const anyEntry = entry as any;
        const sellerProposal =
          anyEntry?.session?.negotiate?.result?.sellerProposal;

        // First check old schema, then new schema
        let discountLines =
          sellerProposal?.discount?.lines ??
          sellerProposal?.merchandiseDiscount?.lines;

        if (
          sellerProposal &&
          discountLines &&
          discountLines.length > 0
        ) {
          discountCode = discountLines[0]?.discount?.code;
          discountAmount = parseFloat(
            discountLines[0]?.lineAmount?.value?.amount,
          );
          originalAmount = parseFloat(
            sellerProposal?.subtotalBeforeReductions?.value ??
              sellerProposal?.subtotalBeforeTaxesAndShipping?.value
                ?.amount,
          );
          break;
        }
      }

      if (discountCode && discountAmount) {
        return {
          discountAmount,
          discountCode,
          originalAmount,
          success: true,
        };
      } else {
        return {
          success: false,
        };
      }
    }

    // VERSION 0
    if(version === "v0") {
      let discountCode: string;
      let discountAmount: number;
      let originalAmount: number;
      let shippingAmount: number;
      let totalAmount: number;
      let success: boolean;
      let subtotalAmount: number;

      discountCode = htmlQsi(
        'span.reduction-code .reduction-code__text',
      );
      const originalPrice = htmlQsi('.total-recap__original-price');
      const shippingPrice = htmlQs(
        '[data-checkout-total-shipping-target]',
      );
      const totalPrice = htmlQsi(
        '[data-checkout-payment-due-target]',
      );
      const subtotalPrice = htmlQsi(
        '[data-checkout-subtotal-price-target]',
      );

      if (
        discountCode &&
        discountCode.toLowerCase() === discount.toLowerCase()
      ) {
        success = true;
        const parsedOriginalAmount = parseCurrency(originalPrice);
        const parsedTotalAmount = parseCurrency(totalPrice);
        const parsedShippingAmount = shippingPrice
          ? parseFloat(
              shippingPrice.getAttribute(
                'data-checkout-total-shipping-target',
              ),
            ) / 100
          : 0.0;
        const parsedSubtotalAmount = parseCurrency(subtotalPrice);
        shippingAmount = isNaN(parsedShippingAmount)
          ? 0.0
          : parsedShippingAmount;
        discountAmount = parsedOriginalAmount - parsedTotalAmount;
        originalAmount = parsedOriginalAmount - shippingAmount;
        totalAmount = parsedTotalAmount - shippingAmount;
        subtotalAmount = parsedSubtotalAmount;
      } else {
        success = false;
      }

      return {
        discountCode,
        discountAmount,
        originalAmount,
        success,
      };
    }
  }
}

/**
 * Shopify Cart API.
 */
export class Cart extends Shopify {
  /**
   * Cached cart data.
   */
  private static data: IHashTable;

  /**
   * Get the cart object.
   * @param cachedVersion - Cached cart or not.
   */
  static async get(
    cachedVersion: boolean = false,
  ): Promise<IHashTable> {
    if (cachedVersion && Cart.data) {
      // Send the cached version
      return Cart.data;
    }

    try {
      // Do a new request
      const response = await Cart.request({ endpoint: '/cart.js' });
      Cart.data = transformResponse(response.response);

      return Cart.data;
    } catch (xhr) {
      return {
        response: xhr.response,
        status: xhr.status,
      };
    }
  }

  /**
   * Adds an item to the cart.
   * @param data - The data to add.
   */
  static async add(
    data: IHashTable | IHashTable[],
  ): Promise<IHashTable> {
    if (data.constructor === Array) {
      // Sequental adding of products to prevent racing
      const tasks = [];
      data.forEach((cartData: IHashTable) => {
        tasks.push(() =>
          Cart.request({
            method: 'POST',
            endpoint: '/cart/add.js',
            data: cartData,
          }),
        );
      });
      tasks.push(() => Cart.get());

      return tasks.reduce(
        (p, task) => p.then(task),
        Promise.resolve(),
      );
    }

    await Cart.request({
      data,
      method: 'POST',
      endpoint: '/cart/add.js',
    });

    return Cart.get();
  }

  /**
   * Updates the cart.
   * @param updates - The data to update (id => qty | [qty, qty, ...]).
   * @param note - The note to pass along.
   */
  static async update(
    updates: IHashTable | number[],
    note?: string,
  ): Promise<IHashTable> {
    const response = await Cart.request({
      method: 'POST',
      endpoint: '/cart/update.js',
      data: { updates, note },
    });

    return transformResponse(response.response);
  }

  /**
   * Updates the cart attibutes.
   * @param attributes - The attribute to push.
   * @param notes - The note to pass along.
   */
  static async attributes(
    attributes: IHashTable,
    note?: string,
  ): Promise<IHashTable> {
    const response = await Cart.request({
      method: 'POST',
      endpoint: '/cart/update.js',
      data: { attributes, note },
    });

    return transformResponse(response.response);
  }

  /**
   * Updates the cart note.
   * @param note - The cart note.
   */
  static async note(note: string): Promise<IHashTable> {
    const response = await Cart.request({
      method: 'POST',
      endpoint: '/cart/update.js',
      data: { note },
    });

    return transformResponse(response.response);
  }

  /**
   * Changes a cart item.
   * @param data - The data to change.
   */
  static async change(data: IHashTable): Promise<IHashTable> {
    const response = await Cart.request({
      data,
      method: 'POST',
      endpoint: '/cart/change.js',
    });

    return transformResponse(response.response);
  }

  /**
   * Removes an item from the cart.
   * @params id - The ID to remove.
   */
  static async remove(id: number): Promise<IHashTable> {
    const data = {};
    data[id] = 0;

    return Cart.update(data);
  }

  /**
   * Removes an item from the cart by line index.
   * @params line - The line index to remove.
   */
  static async removeByLine(line: number): Promise<IHashTable> {
    return Cart.change({ line, quantity: 0 });
  }

  /**
   * Clears the cart.
   */
  static async clear(): Promise<XMLHttpRequest> {
    return Cart.request({
      method: 'POST',
      endpoint: '/cart/clear.js',
    });
  }
}

/**
 * Shopify Product API.
 */
export class Product extends Shopify {
  /**
   * Get the product by handle.
   * @param handle - The product handle.
   */
  static async get(handle: string): Promise<IHashTable | undefined> {
    try {
      const response = await this.request({
        endpoint: `/products/${handle}.json`,
      });
      return transformResponse(response.response).product;
    } catch (err) {
      return undefined;
    }
  }
}

/**
 * Shopify Collection API.
 */
export class Collection extends Shopify {
  /**
   * Get the collection by handle.
   * @param  handle - The collection handle.
   */
  static async get(handle: string): Promise<IHashTable | undefined> {
    try {
      const response = await this.request({
        endpoint: `/collections/${handle}.json`,
      });
      return transformResponse(response.response).collection;
    } catch (err) {
      return undefined;
    }
  }

  /**
   * Get the products in a collection.
   * @param handle - The collection handle.
   */
  static async getProducts(
    handle: string,
  ): Promise<IHashTable | undefined> {
    try {
      const response = await this.request({
        endpoint: `/collections/${handle}/products.json`,
      });
      return transformResponse(response.response).products;
    } catch (err) {
      return undefined;
    }
  }
}

/**
 * Helper functions
 */

export function parseCurrency(currency: string) {
  let number = NaN;
  try {
    const cur_re = /\D*(\d+|\d.*?\d)(?:\D+(\d{2}))?\D*$/;
    const parts = cur_re.exec(currency);
    if (parts && parts[1]) {
      number = parseFloat(
        parts[1].replace(/\D/, '') +
          '.' +
          (parts[2] ? parts[2] : '00'),
      );
    }
  } catch (e) {
    console.error(e);
  } finally {
    return number;
  }
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseCurrency = exports.Collection = exports.Product = exports.Cart = exports.Checkout = void 0;
/**
 * Ensure we get back JSON object from an XHR request.
 * @param response - XHR response data.
 */
function transformResponse(response) {
    // tslint:disable-next-line
    return response.constructor === String
        ? JSON.parse(response)
        : response;
}
/**
 * Shopify API.
 */
class Shopify {
    /**
     * Sends a request to cart.
     * @param params - The params for the registry.
     * @param params.method - The HTTP method.
     * @param params.endpoint - The endpoint of the URL.
     * @param params.data - The data to send to the endpoint.
     * @param params.type - The type of request.
     */
    static request({ method = 'GET', endpoint, data, type = 'json', }) {
        return new Promise((resolve, reject) => {
            // Generate the request object
            const xhr = new XMLHttpRequest();
            let adjustedEndpoint = endpoint;
            if (method === 'GET') {
                adjustedEndpoint += `?${Math.random()
                    .toString(36)
                    .substr(2, 10)}`;
            }
            // Build the request
            xhr.open(method, `${Shopify.baseURI}${adjustedEndpoint}`, true);
            xhr.responseType = type;
            if (type === 'json') {
                xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
            }
            // Resolve or rejust the promise, passing XHR to the promise
            xhr.onload = () => {
                if (xhr.status === 200) {
                    resolve(xhr);
                }
                else {
                    reject(xhr);
                }
            };
            // Send the data
            xhr.send(data ? JSON.stringify(data) : null);
        });
    }
}
/**
 * The base URL.
 */
Shopify.baseURI = `${window.location.protocol}//${window.location.hostname}`;
/**
 * Shopify Checkout API.
 */
class Checkout extends Shopify {
    /**
     * Gets the checkout markup.
     */
    static async checkout() {
        const response = await Checkout.request({
            endpoint: '/checkout',
            type: 'text',
        });
        return response.responseText;
    }
    /**
     * Applies a discount via URL.
     * @param discount - The discount code to apply.
     */
    static async applyDiscount(discount) {
        /*
         * Use HEAD so we don't follow redirects.
         * Shopify seems to set some sort of server var to tell checkout
         * to apply the discount (not the discount_code cookie either).
         */
        return Checkout.request({
            method: 'HEAD',
            endpoint: `/discount/${discount}`,
        });
    }
    /**
     * Verifies the discount code.
     * @param discount - The discount code to verify.
     */
    static async verifyDiscount(discount) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
        // Parse the DOM, easy way
        const response = await Checkout.checkout();
        const parser = new DOMParser();
        const htmlDoc = parser.parseFromString(response, 'text/html');
        console.log(htmlDoc);
        window.htmlDoc = htmlDoc;
        /**
         * Query selector shortcut.
         * @param query - The query string.
         * @param target - The node target.
         */
        const htmlQs = (query, target = htmlDoc) => {
            return target.querySelector(query);
        };
        /**
         * Query selector innerHTML trim shortcut.
         * @param query - The query string.
         * @param target The node target.
         */
        const htmlQsi = (query, target = htmlDoc) => {
            const elem = target.querySelector(query);
            return elem ? elem.innerHTML.trim() : undefined;
        };
        /**
         * Query selector all shortcut.
         * @param query - The query string.
         * @param target - The node target.
         */
        const htmlQsa = (query, target = htmlDoc) => {
            return target.querySelectorAll(query);
        };
        // Check if the store uses the latest checkout, if so use the new handling method
        if (response.includes('data-serialized-id="graphql-queries"')) {
            const getSerializedData = (id) => {
                const element = htmlQs(`[data-serialized-id='${id}']`);
                if (element) {
                    return JSON.parse(element.getAttribute("data-serialized-value"));
                }
            };
            const graphqlEndpoint = getSerializedData("graphql-endpoint");
            const graphqlQueries = getSerializedData("graphql-queries");
            const sessionToken = getSerializedData("session-token");
            const queueToken = getSerializedData("queue-token");
            const checkpointData = getSerializedData("checkpoint-data");
            const urlSearchParams = new URLSearchParams(window.location.search);
            const queryParams = Object.fromEntries(urlSearchParams.entries());
            const getData = function (query) {
                const request = new XMLHttpRequest();
                request.open('POST', graphqlEndpoint);
                request.setRequestHeader('Content-Type', 'application/json');
                const variables = {
                    sessionToken: sessionToken,
                };
                if (query.name === 'NegotiateFromSession') {
                    variables.queueToken = queueToken;
                    variables.checkpointData = checkpointData;
                }
                let resolve;
                let reject;
                const promise = new Promise((res, rej) => {
                    resolve = res;
                    reject = rej;
                });
                request.onload = () => {
                    if (this.status >= 200 && this.status < 300) {
                        resolve(request.response);
                    }
                    else {
                        reject({
                            status: this.status,
                            statusText: request.statusText
                        });
                    }
                };
                request.send(JSON.stringify({
                    query: query.source,
                    variables,
                    operationName: query.name,
                    appBootstrap: true,
                    ...queryParams
                }));
                return promise;
            };
            const data = await getData("NegotiateFromSession");
            window.resultdata = data;
            console.log("resultdata", data);
        }
        else if (response.includes('data-serialized-id="graphql"')) {
            // Check if the store is using the new checkout, if so use the new handling method
            // New checkout version
            const data = JSON.parse(htmlQs('[data-serialized-id="graphql"]').dataset
                .serializedValue);
            // Find the relevant data in the graphql response
            let discountCode;
            let discountAmount;
            let originalAmount;
            for (const entry of Object.values(data)) {
                const anyEntry = entry;
                const sellerProposal = (_c = (_b = (_a = anyEntry === null || anyEntry === void 0 ? void 0 : anyEntry.session) === null || _a === void 0 ? void 0 : _a.negotiate) === null || _b === void 0 ? void 0 : _b.result) === null || _c === void 0 ? void 0 : _c.sellerProposal;
                // First check old schema, then new schema
                let discountLines = (_e = (_d = sellerProposal === null || sellerProposal === void 0 ? void 0 : sellerProposal.discount) === null || _d === void 0 ? void 0 : _d.lines) !== null && _e !== void 0 ? _e : (_f = sellerProposal === null || sellerProposal === void 0 ? void 0 : sellerProposal.merchandiseDiscount) === null || _f === void 0 ? void 0 : _f.lines;
                if (sellerProposal && discountLines && discountLines.length > 0) {
                    discountCode = (_h = (_g = discountLines[0]) === null || _g === void 0 ? void 0 : _g.discount) === null || _h === void 0 ? void 0 : _h.code;
                    discountAmount = parseFloat((_l = (_k = (_j = discountLines[0]) === null || _j === void 0 ? void 0 : _j.lineAmount) === null || _k === void 0 ? void 0 : _k.value) === null || _l === void 0 ? void 0 : _l.amount);
                    originalAmount = parseFloat((_o = (_m = sellerProposal === null || sellerProposal === void 0 ? void 0 : sellerProposal.subtotalBeforeTaxesAndShipping) === null || _m === void 0 ? void 0 : _m.value) === null || _o === void 0 ? void 0 : _o.amount);
                    break;
                }
            }
            if (discountCode && discountAmount) {
                return {
                    discountAmount,
                    discountCode,
                    originalAmount,
                    success: true
                };
            }
            else {
                return {
                    success: false
                };
            }
        }
        else {
            let discountCode;
            let discountAmount;
            let originalAmount;
            let shippingAmount;
            let totalAmount;
            let success;
            let subtotalAmount;
            discountCode = htmlQsi('span.reduction-code .reduction-code__text');
            const originalPrice = htmlQsi('.total-recap__original-price');
            const shippingPrice = htmlQs('[data-checkout-total-shipping-target]');
            const totalPrice = htmlQsi('[data-checkout-payment-due-target]');
            const subtotalPrice = htmlQsi('[data-checkout-subtotal-price-target]');
            if (discountCode &&
                discountCode.toLowerCase() === discount.toLowerCase()) {
                success = true;
                const parsedOriginalAmount = parseCurrency(originalPrice);
                const parsedTotalAmount = parseCurrency(totalPrice);
                const parsedShippingAmount = shippingPrice
                    ? parseFloat(shippingPrice.getAttribute('data-checkout-total-shipping-target')) / 100
                    : 0.0;
                const parsedSubtotalAmount = parseCurrency(subtotalPrice);
                shippingAmount = isNaN(parsedShippingAmount)
                    ? 0.0
                    : parsedShippingAmount;
                discountAmount = parsedOriginalAmount - parsedTotalAmount;
                originalAmount = parsedOriginalAmount - shippingAmount;
                totalAmount = parsedTotalAmount - shippingAmount;
                subtotalAmount = parsedSubtotalAmount;
            }
            else {
                success = false;
            }
            return {
                discountCode,
                discountAmount,
                originalAmount,
                success,
            };
        }
    }
}
exports.Checkout = Checkout;
/**
 * Shopify Cart API.
 */
class Cart extends Shopify {
    /**
     * Get the cart object.
     * @param cachedVersion - Cached cart or not.
     */
    static async get(cachedVersion = false) {
        if (cachedVersion && Cart.data) {
            // Send the cached version
            return Cart.data;
        }
        try {
            // Do a new request
            const response = await Cart.request({ endpoint: '/cart.js' });
            Cart.data = transformResponse(response.response);
            return Cart.data;
        }
        catch (xhr) {
            return {
                response: xhr.response,
                status: xhr.status,
            };
        }
    }
    /**
     * Adds an item to the cart.
     * @param data - The data to add.
     */
    static async add(data) {
        if (data.constructor === Array) {
            // Sequental adding of products to prevent racing
            const tasks = [];
            data.forEach((cartData) => {
                tasks.push(() => Cart.request({
                    method: 'POST',
                    endpoint: '/cart/add.js',
                    data: cartData,
                }));
            });
            tasks.push(() => Cart.get());
            return tasks.reduce((p, task) => p.then(task), Promise.resolve());
        }
        await Cart.request({
            data,
            method: 'POST',
            endpoint: '/cart/add.js',
        });
        return Cart.get();
    }
    /**
     * Updates the cart.
     * @param updates - The data to update (id => qty | [qty, qty, ...]).
     * @param note - The note to pass along.
     */
    static async update(updates, note) {
        const response = await Cart.request({
            method: 'POST',
            endpoint: '/cart/update.js',
            data: { updates, note },
        });
        return transformResponse(response.response);
    }
    /**
     * Updates the cart attibutes.
     * @param attributes - The attribute to push.
     * @param notes - The note to pass along.
     */
    static async attributes(attributes, note) {
        const response = await Cart.request({
            method: 'POST',
            endpoint: '/cart/update.js',
            data: { attributes, note },
        });
        return transformResponse(response.response);
    }
    /**
     * Updates the cart note.
     * @param note - The cart note.
     */
    static async note(note) {
        const response = await Cart.request({
            method: 'POST',
            endpoint: '/cart/update.js',
            data: { note },
        });
        return transformResponse(response.response);
    }
    /**
     * Changes a cart item.
     * @param data - The data to change.
     */
    static async change(data) {
        const response = await Cart.request({
            data,
            method: 'POST',
            endpoint: '/cart/change.js',
        });
        return transformResponse(response.response);
    }
    /**
     * Removes an item from the cart.
     * @params id - The ID to remove.
     */
    static async remove(id) {
        const data = {};
        data[id] = 0;
        return Cart.update(data);
    }
    /**
     * Removes an item from the cart by line index.
     * @params line - The line index to remove.
     */
    static async removeByLine(line) {
        return Cart.change({ line, quantity: 0 });
    }
    /**
     * Clears the cart.
     */
    static async clear() {
        return Cart.request({
            method: 'POST',
            endpoint: '/cart/clear.js',
        });
    }
}
exports.Cart = Cart;
/**
 * Shopify Product API.
 */
class Product extends Shopify {
    /**
     * Get the product by handle.
     * @param handle - The product handle.
     */
    static async get(handle) {
        try {
            const response = await this.request({
                endpoint: `/products/${handle}.json`,
            });
            return transformResponse(response.response).product;
        }
        catch (err) {
            return undefined;
        }
    }
}
exports.Product = Product;
/**
 * Shopify Collection API.
 */
class Collection extends Shopify {
    /**
     * Get the collection by handle.
     * @param  handle - The collection handle.
     */
    static async get(handle) {
        try {
            const response = await this.request({
                endpoint: `/collections/${handle}.json`,
            });
            return transformResponse(response.response).collection;
        }
        catch (err) {
            return undefined;
        }
    }
    /**
     * Get the products in a collection.
     * @param handle - The collection handle.
     */
    static async getProducts(handle) {
        try {
            const response = await this.request({
                endpoint: `/collections/${handle}/products.json`,
            });
            return transformResponse(response.response).products;
        }
        catch (err) {
            return undefined;
        }
    }
}
exports.Collection = Collection;
/**
 * Helper functions
 */
function parseCurrency(currency) {
    let number = NaN;
    try {
        const cur_re = /\D*(\d+|\d.*?\d)(?:\D+(\d{2}))?\D*$/;
        const parts = cur_re.exec(currency);
        if (parts && parts[1]) {
            number = parseFloat(parts[1].replace(/\D/, '') +
                '.' +
                (parts[2] ? parts[2] : '00'));
        }
    }
    catch (e) {
        console.error(e);
    }
    finally {
        return number;
    }
}
exports.parseCurrency = parseCurrency;
